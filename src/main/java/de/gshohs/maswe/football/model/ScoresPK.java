package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Data
public class ScoresPK implements Serializable {

    private Long playerId;
    private InstancePK instancePK;

    public ScoresPK() {
    }

    public ScoresPK(Long playerId, InstancePK instancePK) {
        this.playerId = playerId;
        this.instancePK = instancePK;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScoresPK)) return false;
        ScoresPK scoresPK = (ScoresPK) o;
        return playerId.equals(scoresPK.playerId) &&
                instancePK.equals(scoresPK.instancePK);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, instancePK);
    }
}
