package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Scores {

    @EmbeddedId
    private ScoresPK id;

    @ManyToOne
    @JoinColumn(name = "player_id")
    @MapsId(value = "playerId")
    private Player player;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "instance_id", referencedColumnName = "instance_id"),
            @JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id")
    })
    @MapsId(value = "instancePK")
    private Instance instance;

    private Boolean onwGoal;

    @Temporal(TemporalType.TIMESTAMP)
    private Date occurence;
}
