package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEAM_ID_GEN")
    @SequenceGenerator(name = "TEAM_ID_GEN" ,sequenceName = "TEAM_ID_SEQ", initialValue = 1)
    private Long id;

    @Column(length = 80, nullable = false, unique = true)
    private String title;

    @OneToMany(mappedBy = "team")
    private Set<Player> players;
}
