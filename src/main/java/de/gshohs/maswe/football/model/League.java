package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class League {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LEAGUE_ID_GEN")
    @SequenceGenerator(name = "LEAGUE_ID_GEN" ,sequenceName = "LEAGUE_ID_SEQ", initialValue = 1)
    private Long id;

    @Column(length = 120, nullable = false, unique = true)
    private String title;
}
