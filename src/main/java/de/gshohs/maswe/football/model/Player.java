package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "P")
@Data
public class Player extends Person {

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;
}
