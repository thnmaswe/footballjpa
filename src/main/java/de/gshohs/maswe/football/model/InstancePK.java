package de.gshohs.maswe.football.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class InstancePK implements Serializable {

    private Long scheduleId;

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INSTANCE_ID_GEN")
    @SequenceGenerator(name = "INSTANCE_ID_GEN" ,sequenceName = "INSTANCE_ID_SEQ", initialValue = 1)
    @Column(name = "instance_id")
    private Long instanceId;

    public InstancePK() {
    }

    public InstancePK(Long scheduleId, Long instanceId) {
        this.scheduleId = scheduleId;
        this.instanceId = instanceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InstancePK)) return false;
        InstancePK that = (InstancePK) o;
        return scheduleId.equals(that.scheduleId) &&
                instanceId.equals(that.instanceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheduleId, instanceId);
    }
}
