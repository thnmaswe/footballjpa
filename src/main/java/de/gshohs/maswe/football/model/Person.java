package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "TYPE", length = 1, discriminatorType = DiscriminatorType.STRING)
@Data
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSON_ID_GEN")
    @SequenceGenerator(name = "PERSON_ID_GEN" ,sequenceName = "PERSON_ID_SEQ", initialValue = 1)
    private Long id;

    @Column(length = 20, nullable = false)
    private String givenname;

    @Column(length = 80, nullable = false)
    private String surname;
}
