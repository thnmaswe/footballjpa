package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints =
  @UniqueConstraint(name = "SCHEDULE_UK",
          columnNames = {"season_id", "league_id", "team_home_id", "team_guest_id"}
  )
)
@Data
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SCHEDULE_ID_GEN")
    @SequenceGenerator(name = "SCHEDULE_ID_GEN" ,sequenceName = "SCHEDULE_ID_SEQ", initialValue = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "season_id", nullable = false)
    private Season season;

    @ManyToOne
    @JoinColumn(name = "league_id", nullable = false)
    private League league;

    @ManyToOne
    @JoinColumn( name = "team_home_id", nullable = false)
    private Team teamHome;

    @ManyToOne
    @JoinColumn(name = "team_guest_id", nullable = false)
    private Team teamGuest;
}
