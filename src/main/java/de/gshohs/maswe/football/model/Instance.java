package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Instance {

    @EmbeddedId
    private InstancePK id;

    @ManyToOne
    @JoinColumn(name = "schedule_id")
    @MapsId(value = "scheduleId")
    private Schedule schedule;

    @ManyToOne
    @JoinColumn(name = "refereeId")
    private Referee referee;

}
