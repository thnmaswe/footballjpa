package de.gshohs.maswe.football.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Season {

    @Id
    @Column(precision = 4)
    private Integer year;
}
